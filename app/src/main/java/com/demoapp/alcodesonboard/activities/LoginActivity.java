package com.demoapp.alcodesonboard.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.LoginFragment;

import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(LoginFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, LoginFragment.newInstance(), LoginFragment.TAG)
                    .commit();
        }
    }
}
