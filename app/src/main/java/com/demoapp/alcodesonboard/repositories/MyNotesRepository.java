package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.database.entities.MyNoteDao;
import com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class MyNotesRepository {

    private static MyNotesRepository mInstance;

    private MutableLiveData<List<MyNotesAdapter.DataHolder>> mMyNotesAdapterListLiveData = new MutableLiveData<>();

    public static MyNotesRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyNotesRepository.class) {
                mInstance = new MyNotesRepository();
            }
        }

        return mInstance;
    }

    private MyNotesRepository() {
    }

    public LiveData<List<MyNotesAdapter.DataHolder>> getMyNotesAdapterListLiveData() {
        return mMyNotesAdapterListLiveData;
    }

    public void loadMyNotesAdapterList(Context context) {
        List<MyNotesAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyNote> records = DatabaseHelper.getInstance(context)
                .getMyNoteDao()
                .loadAll();

        if (records != null) {
            for (MyNote myNote : records) {
                MyNotesAdapter.DataHolder dataHolder = new MyNotesAdapter.DataHolder();
                dataHolder.id = myNote.getId();
                dataHolder.title = myNote.getTitle();

                dataHolders.add(dataHolder);
            }
        }

        mMyNotesAdapterListLiveData.setValue(dataHolders);
    }

    public void addNote(Context context, String title, String content) {
        // Create new record.
        MyNote myNote = new MyNote();
        myNote.setTitle(title);
        myNote.setContent(content);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getMyNoteDao()
                .insert(myNote);

        // Done adding record, now re-load list.
        loadMyNotesAdapterList(context);
    }

    public void editNote(Context context, Long id, String title, String content) {
        MyNoteDao myNoteDao = DatabaseHelper.getInstance(context).getMyNoteDao();
        MyNote myNote = myNoteDao.load(id);

        // Check if record exists.
        if (myNote != null) {
            // Record is found, now update.
            myNote.setTitle(title);
            myNote.setContent(content);

            myNoteDao.update(myNote);

            // Done editing record, now re-load list.
            loadMyNotesAdapterList(context);
        }
    }

    public void deleteNote(Context context, Long id) {
        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getMyNoteDao()
                .deleteByKey(id);

        // Done deleting record, now re-load list.
        loadMyNotesAdapterList(context);
    }
}
