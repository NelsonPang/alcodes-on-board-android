package com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;

import java.util.List;

public class MyNotesViewModel extends AndroidViewModel {

    private MyNotesRepository mMyNotesRepository;

    public MyNotesViewModel(@NonNull Application application) {
        super(application);

        mMyNotesRepository = MyNotesRepository.getInstance();
    }

    public LiveData<List<MyNotesAdapter.DataHolder>> getMyNotesAdapterListLiveData() {
        return mMyNotesRepository.getMyNotesAdapterListLiveData();
    }

    public void loadMyNotesAdapterList() {
        mMyNotesRepository.loadMyNotesAdapterList(getApplication());
    }

    public void addNote(String title, String content) {
        mMyNotesRepository.addNote(getApplication(), title, content);
    }

    public void editNote(Long id, String title, String content) {
        mMyNotesRepository.editNote(getApplication(), id, title, content);
    }

    public void deleteNote(Long id) {
        mMyNotesRepository.deleteNote(getApplication(), id);
    }
}


