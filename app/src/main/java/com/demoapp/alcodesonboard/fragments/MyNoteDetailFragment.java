package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;
    private MyNotesViewModel mViewModel;

    public MyNoteDetailFragment() {
    }

    public static MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyNoteDetailFragment fragment = new MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note.
        MenuItem item = menu.findItem(R.id.menu_save);
        item.setTitle("Create");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        // TODO check title and content is blank or not.

        String title = mEditTextTitle.getText().toString();
        String content = mEditTextContent.getText().toString();

        if (title.isEmpty() || content.isEmpty()) {
            Toast.makeText(getActivity().getApplicationContext(), "Title or content should not be empty!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue.

            new MaterialDialog.Builder(getActivity())
                    .title("Create new note")
                    .content("Are you sure you want to create a new note?")
                    .positiveText("Yes")
                    .negativeText("No")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            // Save record and return to list.
                            MyNote myNote = new MyNote();
                            myNote.setTitle(title);
                            myNote.setContent(content);

                            // TODO BAD practice, should move Database operations to Repository.
                            if (mMyNoteId > 0) {
                                // Update record.
                                myNote.setId(mMyNoteId);
//                                    DatabaseHelper.getInstance(getActivity())
//                                            .getMyNoteDao()
//                                            .save(myNote);
//                                    MyNotesRepository.getInstance().editNote(getContext(), mMyNoteId, title, content);
                                mViewModel.editNote(mMyNoteId, title, content);
                            } else {
                                // Create record.
//                                    DatabaseHelper.getInstance(getActivity())
//                                            .getMyNoteDao()
//                                            .insert(myNote);
//                                    MyNotesRepository.getInstance().addNote(getContext(), title, content);
                                mViewModel.addNote(title, content);
                            }

//                            Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

                            getActivity().setResult(MyNoteDetailActivity.RESULT_CONTENT_MODIFIED);
                            getActivity().finish();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        }
                    })
                    .show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        //TODO BAD practice, should move Database operations to Repository.

//        mViewModel.loadMyNotesAdapterList();

        //        MyNotesRepository.getInstance().loadMyNotesAdapterList(getContext());
        if (mMyNoteId > 0) {
            MyNote myNote = DatabaseHelper.getInstance(getActivity())
                    .getMyNoteDao()
                    .load(mMyNoteId);

            if (myNote != null) {
                mEditTextTitle.setText(myNote.getTitle());
                mEditTextContent.setText(myNote.getContent());
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }

    private void initViewModel() {
//        mViewModel = ViewModelProviders.of(this).get(MyNotesViewModel.class);
        mViewModel = new ViewModelProvider(this, new MyNotesViewModelFactory(getActivity().getApplication())).get(MyNotesViewModel.class);

        // Load data into adapter.
        mViewModel.loadMyNotesAdapterList();
    }
}
