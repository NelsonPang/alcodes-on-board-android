package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MainActivity;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_password)
    protected TextInputEditText mEditTextPasword;

    @BindView(R.id.button_login)
    protected MaterialButton mButtonLogin;

    private Unbinder mUnbinder;

//    public static final String PREFS_NAME = "Shared";

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPasword.setText("cityslicka");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick(R.id.button_login)
    protected void doLogin() {
        // Disable button and wait for server response.
        mButtonLogin.setEnabled(false);

        // TODO change login button's label to "Loading"
        // TODO remember reset label to "Login" when needed.

        String originallabel = getString(R.string.originallabel);
        String onclicklabel = getString(R.string.onclicklabel);
        final String changebacklabel = originallabel;

        originallabel = onclicklabel;

        mButtonLogin.setText(originallabel);

        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPasword.getText().toString(); // Password should not trim.

        // TODO check email and password is blank or not.
        // TODO show error message and stop process if validation failed.

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(getActivity().getApplicationContext(), "Email or password should not be empty!", Toast.LENGTH_SHORT).show();
            mButtonLogin.setText(changebacklabel);
            mButtonLogin.setEnabled(true);
            return;
        }

//        if (!email.contains("@"))
//        {
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    Toast.makeText(getActivity().getApplicationContext(), "Invalid email!", Toast.LENGTH_SHORT).show();
//                }
//            }, 2000);
//        }

        // Call login API.
        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();

                // Convert JSON string to Java object.
                LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                // Save user's email to shared preference.
                SharedPreferenceHelper.getInstance(getActivity())
                        .edit()
                        .putString("email", email)
                        .putString("token", responseModel.token)
                        .apply();

                // TODO bad practice, should check user is still staying in this app or not after server response.

                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                    String loggedIn = SharedPreferenceHelper.getInstance(getActivity()).getString("email", "");

                    if (loggedIn != null || loggedIn != "") {
                        Toast.makeText(getActivity().getApplicationContext(), "Logged in!", Toast.LENGTH_SHORT).show();
                    }
                }

//                SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
//                boolean variablex = settings.getBoolean("loggedIn", false);
//
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("loggedIn", true);
//
//                editor.commit();

                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("d;;Login error: %s", error.getMessage());

                // TODO bad practice, should check user is still staying in this app or not after server response.

                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                    // Show error in popup dialog.
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Invalid email or password.")
                            .positiveText("OK")
                            .show();
                }

//                String loggedIn = SharedPreferenceHelper.getInstance(getActivity()).getString("email", "");
//
//                if(loggedIn == null||loggedIn == ""){
//                    Handler handler1 = new Handler();
//                    handler1.postDelayed(new Runnable()
//                    {
//                        @Override
//                        public void run()
//                        {
//                            Toast.makeText(getActivity().getApplicationContext(), "Failed to logged in!", Toast.LENGTH_SHORT).show();
//                        }
//                    }, 3000);
//                }

//                SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
//                boolean variablex = settings.getBoolean("loggedIn", false);
//
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("loggedIn", false);
//
//                editor.commit();

                mButtonLogin.setText(changebacklabel);

                mButtonLogin.setEnabled(true);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

//        try{
//            Thread.sleep(2000);
//        }catch(InterruptedException e){
//            e.printStackTrace();
//        }

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }
}
