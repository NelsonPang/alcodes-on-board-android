package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.activities.MyNotesActivity;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

    public static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.textview_greetings)
    protected TextView mTextViewGreetings;

    private Unbinder mUnbinder;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_logout) {
            // TODO show confirm dialog before continue.

            new MaterialDialog.Builder(getActivity())
                    .title("Logout")
                    .content("Are you sure you want to logout?")
                    .positiveText("Yes")
                    .negativeText("No")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // Logout user.
                            // Clear all user's data.
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();

                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();

                            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                                Toast.makeText(getActivity().getApplicationContext(), "Logged out!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        }
                    })
                    .show();

//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//            builder.setCancelable(true);
//            builder.setTitle("Logout");
//            builder.setMessage("Are you sure you want to logout?");
//            builder.setPositiveButton("Logout",
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // Logout user.
//                            // Clear all user's data.
//                            SharedPreferenceHelper.getInstance(getActivity())
//                                    .edit()
//                                    .clear()
//                                    .apply();
//
//                            // Go to login page.
//                            startActivity(new Intent(getActivity(), LoginActivity.class));
//                            getActivity().finish();
//
//                            if(getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
//                                Toast.makeText(getActivity().getApplicationContext(), "Logged out!", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    });
//            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                }
//            });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.button_my_notes)
    protected void startMyNotesActivity() {
        startActivity(new Intent(getActivity(), MyNotesActivity.class));
    }

    private void initView() {
        // User is logged in.
        // Show greetings message.
        String greetings = SharedPreferenceHelper.getInstance(getActivity()).getString("email", "");

        mTextViewGreetings.setText("Hello " + greetings + "!");
    }
}
